<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/route-1', 'UserController@index')->middleware(['role']);

Route::get('/route-2', 'UserController@index')->middleware(['role:admin']);

Route::get('/route-3', 'UserController@index')->middleware(['role:admin,guest']);
